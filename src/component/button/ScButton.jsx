import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import {
  SC_BUTTON_VISUAL_TYPE_CONFIGS,
  SC_BUTTON_VISUAL_TYPES,
} from "./scButtonVisualTypes";
import s from "./_sc-button.scss";

class ScButton extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    contentClassName: PropTypes.string,
    textClassName: PropTypes.string,
    iconContainerClassName: PropTypes.string,
    iconClassName: PropTypes.string,
    pendingIndicatorClassName: PropTypes.string,
    tabIndex: PropTypes.number,
    text: PropTypes.string,
    hasIcon: PropTypes.bool,
    type: PropTypes.string,
    isDisabled: PropTypes.bool,
    isPending: PropTypes.bool,
    visualType: PropTypes.string,
    onClick: PropTypes.func,
    onMouseDown: PropTypes.func,
  };

  static defaultProps = {
    type: "button",
    hasIcon: false,
    isPending: false,
    tabIndex: 0,
  };

  handleMouseDown = (event) => {
    const {isPending, onMouseDown} = this.props;

    if (!isPending) {
      if (onMouseDown) {
        onMouseDown(event);
      }
    }
  }

  handleClick = (event) => {
    const {
      isPending,
      onClick,
      isDisabled,
    } = this.props;

    if (!isPending && !isDisabled) {
      if (onClick) {
        onClick(event);
      }
    }
  }

  refCallback = (ref) => {
    this.buttonElementRef = ref;
  }

  render() {
    const {
      className,
      contentClassName,
      textClassName,
      iconClassName,
      iconContainerClassName,
      pendingIndicatorClassName,
      text,
      hasIcon,
      tabIndex,
      isDisabled,
      type,
      isPending,
      visualType,
    } = this.props;
    const containerClassName = classNames(
      s.root,
      "sc-button",
      SC_BUTTON_VISUAL_TYPE_CONFIGS[visualType] &&
      s[SC_BUTTON_VISUAL_TYPE_CONFIGS[visualType].className],
      className,
      {
        [s.isPending]: isPending,
      }
    );

    return (
      <button
        ref={this.refCallback}
        className={containerClassName}
        disabled={isDisabled}
        onClick={this.handleClick}
        onMouseDown={this.handleMouseDown}
        tabIndex={tabIndex}
        type={type}>

        <span className={classNames(s.content, contentClassName, "sc-button__content")}>
          {
            !isPending && hasIcon &&
            <span className={classNames(s.iconContainer, iconContainerClassName, "sc-button__icon-container")}>
              <span className={classNames(s.icon, iconClassName, "sc-button__icon")}/>
            </span>
          }

          {
            !isPending &&
            text &&
            <span className={classNames(s.text, textClassName, "sc-button__text")}>
              {text}
            </span>
          }

          {
            isPending &&
            <div className={classNames(s.pendingIndicator, pendingIndicatorClassName, "sc-button__pending-indicator")}/>
          }
        </span>
      </button>
    );
  }
}

export default ScButton;
export {
  SC_BUTTON_VISUAL_TYPES,
};
