import React from "react";
import {shallow} from "enzyme";
import renderer from "react-test-renderer";

import ScButton from "./ScButton";

it("renders one button element in root", () => {
  const wrapper = shallow(<ScButton/>);

  expect(wrapper.get(0).type).toBe("button");
});

it("adds given custom class name to the root", () => {
  const wrapper = shallow(<ScButton className={"my-button"}/>);

  expect(wrapper.first("button").hasClass("my-button")).toBe(true);
});

it("disables the button element when isDisabled is true", () => {
  const wrapper = shallow(<ScButton isDisabled={true}/>);

  expect(wrapper.first("button").prop("disabled")).toBe(true);
});

it("renders 'pending indicator' element when isPending is true", () => {
  const wrapper = shallow(<ScButton isPending={true}/>);

  expect(wrapper.find(".sc-button__pending-indicator")).toHaveLength(1);
});

it("does not render 'pending indicator' element when isPending is false", () => {
  const wrapper = shallow(<ScButton isPending={false}/>);

  expect(wrapper.find(".sc-button__pending-indicator")).toHaveLength(0);
});

it("renders text in '.sc-button__text'", () => {
  const wrapper = shallow(<ScButton text={"My Text"}/>);

  expect(wrapper.find(".sc-button__text")).toHaveLength(1);
  expect(wrapper.first(".sc-button__text").text()).toBe("My Text");
});

it("does not render text when isPending is true", () => {
  const wrapper = shallow(
    <ScButton
      isPending={true}
      text={"My Text"}/>
  );

  expect(wrapper.find(".sc-button__text")).toHaveLength(0);
});

it("does not render icon when isPending is true", () => {
  const wrapper = shallow(
    <ScButton
      isPending={true}
      hasIcon={true}/>
  );

  expect(wrapper.find(".sc-button__icon")).toHaveLength(0);
});

it("renders 'icon-container' and 'icon' when hasIcon is true", () => {
  const wrapper = shallow(<ScButton hasIcon={true}/>);

  expect(wrapper.find(".sc-button__icon-container")).toHaveLength(1);
  expect(wrapper.find(".sc-button__icon")).toHaveLength(1);
});

it("correctly sets the type of the button when type is given", () => {
  const wrapper = shallow(<ScButton type={"submit"}/>);

  expect(wrapper.find("button").prop("type")).toBe("submit");
});

it("does not trigger click events when isPending is true", () => {
  const mockClickCallback = jest.fn();

  const wrapper = shallow(
    <ScButton
      isPending={true}
      onClick={mockClickCallback}/>);

  wrapper.simulate("click");
  expect(mockClickCallback.mock.calls.length).toBe(0);
});

// Snapshots
it("renders correctly", () => {
  const tree = renderer
    .create(<ScButton/>)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders correctly with 'icon-container' and 'icon' when hasIcon is true", () => {
  const tree = renderer
    .create(<ScButton hasIcon={true}/>)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders correctly without icon when 'hasIcon' is true but isPending is also true", () => {
  const tree = renderer
    .create(
      <ScButton
        hasIcon={true}
        isPending={true}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders with 'sc-button__text' when text is not empty", () => {
  const tree = renderer
    .create(<ScButton text={"test text"}/>)
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders correctly without text when text is given but isPending is true", () => {
  const tree = renderer
    .create(
      <ScButton
        text={"test text"}
        isPending={true}/>
    )
    .toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders as a non-pending button", () => {
  const tree = renderer
    .create(
      <ScButton
        text={"test text"}
        hasIcon={true}
        isPending={false}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});

it("renders as a pending button", () => {
  const tree = renderer
    .create(
      <ScButton
        text={"test text"}
        hasIcon={true}
        isPending={true}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});

it("button with icon and text", () => {
  const tree = renderer
    .create(
      <ScButton
        text={"test text"}
        hasIcon={true}
        isPending={false}/>
    ).toJSON();

  expect(tree).toMatchSnapshot();
});
