import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import {
  addHtmlClassNames,
  removeHtmlClassNames,
} from "../../../common/dom/domUtils";
import s from "./_sc-page-content.scss";

class ScPageContent extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    toggleClassNames: PropTypes.string,
    children: PropTypes.node,
  }

  componentDidMount() {
    const {toggleClassNames} = this.props;

    if (toggleClassNames) {
      addHtmlClassNames(toggleClassNames);
    }
  }

  componentWillUnmount() {
    const {toggleClassNames} = this.props;

    if (toggleClassNames) {
      removeHtmlClassNames(toggleClassNames);
    }
  }

  render() {
    const {
      children,
      className,
    } = this.props;
    const rootClassName = classNames(
      s.root,
      className,
    );

    return (
      <div className={rootClassName}>
        {children}
      </div>
    );
  }
}

export default ScPageContent;
