import React, {PureComponent} from "react";
import {Route} from "react-router-dom";

/* eslint-disable react/no-multi-comp */
const renderMergedProps = (component, ownRouteProps, routingProps) => {
  const finalProps = {
    ...ownRouteProps,
    ...routingProps,
  };

  return React.createElement(component, finalProps);
};

/* eslint-disable react/jsx-no-bind */
class ScRoute extends PureComponent {
  render() {
    const {component, routingProps, ...rest} = this.props;

    return (
      <Route
        {...rest}
        render={routeProps => renderMergedProps(component, routeProps, routingProps)}/>
    );
  }
}
/* eslint-enable react/jsx-no-bind */
/* eslint-enable react/no-multi-comp */

export default ScRoute;
