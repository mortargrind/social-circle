const actionCreators = {
  resetReduxErrorState() {
    return {
      type: "RESET_REDUX_ERROR_STATE",
      payload: {}
    };
  },

  resetComponentErrorState() {
    return {
      type: "RESET_COMPONENT_ERROR_STATE",
      payload: {}
    };
  },

  resetErrorState() {
    return {
      type: "RESET_ERROR_STATE",
      payload: {}
    };
  },

  reduxErrorOccurred(error, lastAction) {
    return {
      type: "REDUX_ERROR_OCCURRED",
      payload: {
        error,
        lastAction
      },
      isError: true
    };
  },

  componentErrorOccurred(error) {
    return {
      type: "COMPONENT_ERROR_OCCURRED",
      payload: {
        error
      },
      isError: true
    };
  }
};

export {
  actionCreators
};
