function generateDefaultInitialErrorState() {
  return {
    reduxError: null,
    componentError: null
  };
}

function errorReducer(state = generateDefaultInitialErrorState(), action) {
  let newState;

  switch (action.type) {
    case ("REDUX_ERROR_OCCURRED"): {
      console.error(action.payload.error);

      newState = {
        ...state,
        reduxError: action.payload.error
      };

      break;
    }

    case ("COMPONENT_ERROR_OCCURRED"): {
      console.error(action.payload.error);

      newState = {
        ...state,
        componentError: action.payload.error
      };

      break;
    }

    case ("RESET_REDUX_ERROR_STATE"): {
      newState = {
        ...state,
        reduxError: null
      };

      break;
    }

    case ("RESET_COMPONENT_ERROR_STATE"): {
      newState = {
        ...state,
        componentError: null
      };

      break;
    }

    case ("RESET_ERROR_STATE"): {
      newState = {
        ...state,
        reduxError: null,
        componentError: null
      };

      break;
    }

    default:
      newState = state;
  }

  return newState;
}

export {generateDefaultInitialErrorState};
export default errorReducer;
