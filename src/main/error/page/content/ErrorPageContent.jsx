import React, {PureComponent} from "react";

import ScPageContent from "../../../../component/page/content/ScPageContent";

import s from "./_error-page-content.scss";

class ErrorPageContent extends PureComponent {
  render() {
    return (
      <ScPageContent
        className={s.root}
        toggleClassNames={s.page}>

        <div className={s.messageContent}>
          {"Something went wrong :("}
        </div>
      </ScPageContent>
    );
  }
}

export default ErrorPageContent;
