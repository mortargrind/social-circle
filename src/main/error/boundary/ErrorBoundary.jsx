import {PureComponent} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import {actionCreators as errorActionCreators} from "../errorActions";

class ErrorBoundary extends PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    fallbackContent: PropTypes.node,
    reduxError: PropTypes.object,
    componentError: PropTypes.object,
    onError: PropTypes.func
  }

  static defaultProps = {
    fallbackContent: null
  }

  static getDerivedStateFromProps(props) {
    const shouldRenderChildren = !(props.componentError) && !(props.reduxError);

    return {
      shouldRenderChildren,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      shouldRenderChildren: true
    };
  }

  componentDidCatch(error) {
    const {dispatch, onError} = this.props;

    this.setState({
      shouldRenderChildren: false
    });

    dispatch(errorActionCreators.componentErrorOccurred(error));

    if (onError) {
      onError(error);
    }
  }

  render() {
    const {children, fallbackContent} = this.props;
    const {shouldRenderChildren} = this.state;

    return (shouldRenderChildren ? children : fallbackContent);
  }
}

function mapStateToProps(state, ownProps) {
  const {
    errorState: {
      reduxError,
      componentError
    }
  } = state;

  return {
    ...ownProps,
    reduxError,
    componentError,
  };
}

const ConnectedErrorBoundary = connect(mapStateToProps)(ErrorBoundary);

export default ConnectedErrorBoundary;
