import {actionCreators as errorActionCreators} from "../../error/errorActions";

function reduxErrorHandler(error, getState, lastAction, dispatch) {
  dispatch(errorActionCreators.reduxErrorOccurred(error, lastAction));
}

export default reduxErrorHandler;
