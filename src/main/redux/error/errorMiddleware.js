function createErrorMiddleware(errorHandler) {
  return function ({getState, dispatch}) {
    return function (next) {
      return function (action) {
        let result;

        try {
          result = next(action);
        } catch (error) {
          errorHandler(error, getState, action, dispatch);

          result = error;
        }

        return result;
      };
    };
  };
}

export default createErrorMiddleware;
