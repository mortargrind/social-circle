import {
  createStore,
  combineReducers,
  compose,
  applyMiddleware,
} from "redux";

import profileReducer, {
  generateDefaultInitialProfileState
} from "../../profile/profileReducer";
import errorReducer, {
  generateDefaultInitialErrorState
} from "../error/errorReducer";
import createErrorMiddleware from "./error/errorMiddleware";
import reduxErrorHandler from "./error/reduxErrorHandler";

function createInitialReduxStoreState(customInitialState) {
  const initialState = {
    profileState: {
      ...generateDefaultInitialProfileState(),
      ...customInitialState.profileState
    },
    errorState: {
      ...generateDefaultInitialErrorState(),
      ...customInitialState.errorState
    }
  };

  return initialState;
}

function createRootReducer() {
  const reducers = {
    profileState: profileReducer,
    errorState: errorReducer
  };

  return combineReducers(reducers);
}

function createReduxStore(customInitialState = {}) {
  return createStore(
    createRootReducer(),
    createInitialReduxStoreState(customInitialState),
    compose(
      applyMiddleware(
        createErrorMiddleware(reduxErrorHandler)
      )
    )
  );
}

export {
  createReduxStore,
};
