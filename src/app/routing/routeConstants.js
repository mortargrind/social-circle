const ROUTE_PATHS = {};

ROUTE_PATHS.DELETE_PROFILE_PAGE = "/profiles/delete";

const ROUTE_CONFIGS = {
  [ROUTE_PATHS.DELETE_PROFILE_PAGE]: {
    routeName: "DELETE_PROFILE_PAGE",
    isPublic: true,
    exact: true
  }
};

export {
  ROUTE_PATHS,
  ROUTE_CONFIGS
};
