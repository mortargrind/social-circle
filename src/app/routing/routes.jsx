import React from "react";
import {Redirect} from "react-router-dom";

import {
  ROUTE_CONFIGS,
  ROUTE_PATHS
} from "./routeConstants";
import ScRoute from "../../component/route/ScRoute";
import DeleteProfilePageContent from "../../profile/delete/page/content/DeleteProfilePageContent";
import ErrorPageContent from "../../main/error/page/content/ErrorPageContent";

function renderRoutes(routingProps) {
  return [
    (routingProps.componentError || routingProps.reduxError) &&
    <ScRoute
      {...ROUTE_CONFIGS.DELETE_PROFILE_PAGE}
      key={"Error Page"}
      component={ErrorPageContent}
      path={"*"}
      routingProps={routingProps}/>,

    <ScRoute
      {...ROUTE_CONFIGS.DELETE_PROFILE_PAGE}
      key={ROUTE_PATHS.DELETE_PROFILE_PAGE}
      component={DeleteProfilePageContent}
      path={ROUTE_PATHS.DELETE_PROFILE_PAGE}
      routingProps={routingProps}/>,

    <Redirect
      key={"Default Redirect"}
      to={ROUTE_PATHS.DELETE_PROFILE_PAGE}/>
  ];
}

export {
  renderRoutes
};
