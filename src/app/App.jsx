import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Switch} from "react-router-dom";
import {hot} from "react-hot-loader";

import {renderRoutes} from "./routing/routes";

import s from "./app.scss";

class App extends PureComponent {
  static propTypes = {
    reduxError: PropTypes.object,
    componentError: PropTypes.object,
  }

  constructor(props) {
    super(props);

    this.state = {
      hasErrorInRootComponentTree: false,
    };
  }

  generateRoutingProps() {
    const {
      reduxError,
      componentError,
    } = this.props;
    const {hasErrorInRootComponentTree} = this.state;

    return {
      reduxError,
      componentError,
      hasErrorInRootComponentTree,
    };
  }

  render() {
    return (
      <div className={s.root}>
        <Switch>
          {renderRoutes(this.generateRoutingProps())}
        </Switch>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {
    errorState: {
      reduxError,
      componentError,
    }
  } = state;

  return {
    reduxError,
    componentError,
  };
}

const ConnectedApp = connect(mapStateToProps)(App);

export default hot(module)(ConnectedApp);
