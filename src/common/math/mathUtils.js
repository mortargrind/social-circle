const EVEN_MODULO = 2;
const ODD_MODULO = 2;

function isOdd(number) {
  return number >= 0 && (number % ODD_MODULO === 1);
}

function isEven(number) {
  return number >= 0 && (number % EVEN_MODULO === 0);
}

export {
  isOdd,
  isEven,
};
