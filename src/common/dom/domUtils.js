function addMultipleClassNames(element, classes) {
  const classList = classes.split(" ");

  for (let index = 0; index < classList.length; index++) {
    if (classList[index]) {
      element.classList.add(classList[index]);
    }
  }
}

function removeMultipleClassNames(element, classes) {
  const classList = classes.split(" ");

  for (let index = 0; index < classList.length; index++) {
    if (classList[index]) {
      element.classList.remove(classList[index]);
    }
  }
}

function addHtmlClassNames(classNames) {
  const htmlElement = document.querySelector("html");

  addMultipleClassNames(htmlElement, classNames);
}

function removeHtmlClassNames(classNames) {
  const htmlElement = document.querySelector("html");

  removeMultipleClassNames(htmlElement, classNames);
}

export {
  addHtmlClassNames,
  removeHtmlClassNames,
  addMultipleClassNames,
  removeMultipleClassNames,
};
