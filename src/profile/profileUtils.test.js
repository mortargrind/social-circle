import {
  Map as ImmutableMap,
  OrderedMap as ImmutableOrderedMap,
} from "immutable";

import {
  deleteProfileFromContactList,
  generateProfileMapFromRawProfiles,
  generateContactIdsByProfileMapFromProfileMap,
  generateContactPathForProfile,
  generateNestedProfileListFromProfileMap,
  findIndexOfProfile,
} from "./profileUtils";
import Profile from "./Profile";
import {
  largeRawProfileArray,
  profileWith3Contacts,
} from "../test/mock/profile/profileMocks";

/* eslint-disable no-magic-numbers */
describe("profile Utils test", () => {
  it("deleteProfileFromContactList removes contact from parent profile in an immutable way ",
    () => {
      const updatedProfile = deleteProfileFromContactList(
        profileWith3Contacts,
        profileWith3Contacts.contactList.get(1)
      );

      expect(updatedProfile.contactList.size).toBe(2);
      expect(updatedProfile.contactList.toJS()).toEqual(
        [
          profileWith3Contacts.contactList.get(0),
          profileWith3Contacts.contactList.get(2),
        ]
      );
      expect(updatedProfile).not.toBe(profileWith3Contacts);
    });

  it("generateProfileMapFromRawProfiles should generate the profile map correctly",
    () => {
      const profileMap = generateProfileMapFromRawProfiles(largeRawProfileArray);

      expect(Array.from(profileMap.values())).toEqual(
        largeRawProfileArray.map(rawProfile => Profile.fromRawProfile(rawProfile))
      );
      expect(profileMap.size).toBe(10);
      expect(ImmutableOrderedMap.isOrderedMap(profileMap)).toBe(true);
    });

  it("generateContactIdsByProfileMapFromProfileMap should generate the profile map correctly for ",
    () => {
      const profileMap = generateProfileMapFromRawProfiles(largeRawProfileArray);
      const contactIdsByProfileMap = generateContactIdsByProfileMapFromProfileMap(profileMap);

      expect(Array.from(contactIdsByProfileMap.entries()))
        .toEqual([
          [1, [2, 3, 4]],
          [2, [8, 9]],
          [3, [5]],
          [4, []],
          [5, [7]],
          [6, []],
          [7, []],
          [8, []],
          [9, []],
          [10, []],
        ]);
      expect(contactIdsByProfileMap.size).toBe(10);
      expect(ImmutableMap.isMap(contactIdsByProfileMap)).toBe(true);
    });

  it("generateContactPathForProfile finds the path for direct items", () => {
    const candidateProfile = profileWith3Contacts;
    const profileMap = generateProfileMapFromRawProfiles(
      largeRawProfileArray
    );
    const nestedProfileList = generateNestedProfileListFromProfileMap(
      profileMap
    );
    const path = generateContactPathForProfile(
      candidateProfile,
      profileMap,
      nestedProfileList,
    );

    expect(path).toEqual([0]);
  });

  it("generateContactPathForProfile finds the path for nested items", () => {
    const profileMap = generateProfileMapFromRawProfiles(
      largeRawProfileArray
    );
    const candidateProfile = profileMap.get(7);
    const nestedProfileList = generateNestedProfileListFromProfileMap(
      profileMap
    );
    const path = generateContactPathForProfile(
      candidateProfile,
      profileMap,
      nestedProfileList,
    );

    expect(path).toEqual([0, "contactList", 0, "contactList", 1, "contactList", 0]);
  });

  it("generateContactPathForProfile does not find the path for null/undefined references", () => {
    const profileMap = generateProfileMapFromRawProfiles(
      largeRawProfileArray
    );
    const candidateProfile = null;
    const nestedProfileList = generateNestedProfileListFromProfileMap(
      profileMap
    );

    const path = generateContactPathForProfile(
      candidateProfile,
      profileMap,
      nestedProfileList,
    );

    expect(path).toEqual([]);
  });


  it("generateContactPathForProfile does not find the path for non-existent items", () => {
    const profileMap = generateProfileMapFromRawProfiles(
      largeRawProfileArray
    );
    const candidateProfile = {
      id: 14,
      connectedProfileId: 25,
    };
    const nestedProfileList = generateNestedProfileListFromProfileMap(
      profileMap
    );

    const path = generateContactPathForProfile(
      candidateProfile,
      profileMap,
      nestedProfileList,
    );

    expect(path).toEqual([]);
  });

  it("findIndexOfProfile correctly returns first found profile index by Profile.id", () => {
    const profileList = largeRawProfileArray.map(
      rawProfile => Profile.fromRawProfile(rawProfile)
    );
    const index = findIndexOfProfile(
      profileList,
      {
        id: 4,
      }
    );

    expect(index).toBe(3);
  });

  it("findIndexOfProfile correctly returns -1 when given profile is not found by Profile.id", () => {
    const profileList = largeRawProfileArray.map(
      rawProfile => Profile.fromRawProfile(rawProfile)
    );
    const index = findIndexOfProfile(
      profileList,
      {
        id: 14,
      }
    );

    expect(index).toBe(-1);
  });

  it("generateNestedProfileListFromProfileMap correctly generates the nested immutable list with populated contactList collections",
    () => {
      const profileMap = generateProfileMapFromRawProfiles(largeRawProfileArray);
      const nestedProfileList = generateNestedProfileListFromProfileMap(profileMap);

      expect(
        nestedProfileList.toJS()
      ).toEqual(
        [
          {
            id: 1,
            fullName: "Ahmet Hamdi Tanpınar",
            cityName: "İstanbul",
            phoneNumber: "123456",
            connectedProfileId: 12,
            contactList: [
              {
                id: 2,
                fullName: "Sait Faik Abasıyanık",
                cityName: "İstanbul",
                phoneNumber: "123456",
                connectedProfileId: 1,
                contactList: [
                  {
                    id: 8,
                    fullName: "Yaşar Kemal",
                    cityName: "Artvin",
                    phoneNumber: "1313131",
                    connectedProfileId: 2,
                    contactList: [],
                  },
                  {
                    id: 9,
                    fullName: "Murathan Mungan",
                    cityName: "Van",
                    phoneNumber: "1313131",
                    connectedProfileId: 2,
                    contactList: [],
                  }
                ],
              },
              {
                id: 3,
                fullName: "Peyami Safa",
                cityName: "İzmir",
                phoneNumber: "345345",
                connectedProfileId: 1,
                contactList: [
                  {
                    id: 5,
                    fullName: "Kemal Tahir",
                    cityName: "Bursa",
                    phoneNumber: "1313131",
                    connectedProfileId: 3,
                    contactList: [
                      {
                        id: 7,
                        fullName: "Reşat Nuri Güntekin",
                        cityName: "Nevşehir",
                        phoneNumber: "1313131",
                        connectedProfileId: 5,
                        contactList: [],
                      }
                    ]
                  }
                ],
              },
              {
                id: 4,
                fullName: "Ömer Seyfettin",
                cityName: "Ankara",
                phoneNumber: "567890",
                connectedProfileId: 1,
                contactList: [],
              },
            ]
          },
          {
            id: 6,
            fullName: "Sabahattin Ali",
            cityName: "Adana",
            phoneNumber: "1313131",
            connectedProfileId: 0,
            contactList: [],
          },
          {
            id: 10,
            fullName: "Orhan Kemal",
            cityName: "Edirne",
            phoneNumber: "1313131",
            connectedProfileId: null,
            contactList: [],
          }
        ]
      );
    }
  );
});
/* eslint-enable no-magic-numbers */
