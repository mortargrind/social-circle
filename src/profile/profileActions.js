const actionCreators = {
  deleteNestedProfile(profileId) {
    return {
      type: "DELETE_NESTED_PROFILE",
      payload: {
        profileId
      }
    };
  }
};

export {
  actionCreators
};
