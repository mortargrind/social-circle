import {
  List as ImmutableList
} from "immutable";

class Profile {
  static fromRawProfile(rawContact) {
    return {
      id: rawContact.ID,
      fullName: rawContact.Name,
      cityName: rawContact.City,
      phoneNumber: rawContact.Phone,
      connectedProfileId: rawContact.parentID == null ? null : rawContact.parentID,
      contactList: new ImmutableList()
    };
  }
}

export default Profile;
