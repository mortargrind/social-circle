import React from "react";
import {mount} from "enzyme";
import renderer from "react-test-renderer";

import NestedProfileSummaryRow from "./NestedProfileSummaryRow";
import {
  profileWith3Contacts,
  profileWithoutContacts,
  profileWithNestedContacts,
} from "../../../../test/mock/profile/profileMocks";

describe("NestedProfileSummaryRow tests", () => {
  it("calls the given onDeleteClick callback and not cause collapsing toggle", () => {
    const mockDeleteCallback = jest.fn();
    const wrapper = mount(
      <NestedProfileSummaryRow
        profile={profileWithoutContacts}
        onDeleteClick={mockDeleteCallback}/>,
    );

    const instance = wrapper.instance();
    const initialCollapseState = instance.state.isCollapsed;

    wrapper.find(".sc-button").simulate("click");

    expect(mockDeleteCallback).toHaveBeenCalledTimes(1);
    expect(instance.state.isCollapsed).toBe(initialCollapseState);
  });

  it("does toggle the collapse state when it is clicked", () => {
    const mockDeleteCallback = jest.fn();
    const wrapper = mount(
      <NestedProfileSummaryRow
        profile={profileWithoutContacts}
        onDeleteClick={mockDeleteCallback}/>,
    );

    const instance = wrapper.instance();
    const initialCollapseState = instance.state.isCollapsed;

    wrapper.simulate("click");

    expect(mockDeleteCallback).toHaveBeenCalledTimes(0);
    expect(instance.state.isCollapsed).toBe(!initialCollapseState);
  });

  it("renders nested profile row with a contact-less profile object correctly", () => {
    const tree = renderer.create(
      <NestedProfileSummaryRow profile={profileWithoutContacts}/>,
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders nested profile row with a contact-less profile object and a delete callback correctly", () => {
    const tree = renderer.create(
      <NestedProfileSummaryRow
        profile={profileWithoutContacts}
        onDeleteClick={jest.fn()}/>,
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders nested profile row with a 3-contact profile object correctly", () => {
    const tree = renderer.create(
      <NestedProfileSummaryRow
        profile={profileWith3Contacts}/>,
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders nested profile row with a 3-contact profile object and a delete callback correctly", () => {
    const tree = renderer.create(
      <NestedProfileSummaryRow
        profile={profileWith3Contacts}
        onDeleteClick={jest.fn()}/>,
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders nested profile row with a 3-contact profile object and a delete callback correctly", () => {
    const tree = renderer.create(
      <NestedProfileSummaryRow
        profile={profileWith3Contacts}
        onDeleteClick={jest.fn()}/>,
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders nested profile row with a nested-contact profile object correctly", () => {
    const tree = renderer.create(
      <NestedProfileSummaryRow
        profile={profileWithNestedContacts}/>,
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it("renders nested profile row with a nested-contact profile object and a delete callback correctly", () => {
    const tree = renderer.create(
      <NestedProfileSummaryRow
        profile={profileWithNestedContacts}
        onDeleteClick={jest.fn()}/>,
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });
});

