import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import ScButton, {SC_BUTTON_VISUAL_TYPES} from "../../../../component/button/ScButton";
import NestedProfileSummaryRowListView from "./list/view/NestedProfileSummaryRowListView";

import s from "./_nested-profile-summary-row.scss";

class NestedProfileSummaryRow extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    profile: PropTypes.object.isRequired,
    depthLevel: PropTypes.number,
    onDeleteClick: PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.state = {
      isCollapsed: true,
    };
  }

  handleDeleteClick = (event) => {
    const {
      profile,
      onDeleteClick,
    } = this.props;

    event.stopPropagation();

    if (onDeleteClick) {
      onDeleteClick(event, profile);
    }
  }

  handleClick = (event) => {
    const {isCollapsed} = this.state;

    event.stopPropagation();

    this.setState({
      isCollapsed: !isCollapsed,
    });
  }

  render() {
    const {
      className,
      profile,
      depthLevel,
      onDeleteClick,
    } = this.props;
    const {isCollapsed} = this.state;
    const hasContacts = profile.contactList.size > 0;
    const rootClassName = classNames(
      s.root,
      className,
      {
        [s.displayingContacts]: hasContacts && !isCollapsed,
      }
    );

    return (
      <div
        className={rootClassName}
        onClick={this.handleClick}>
        <div className={s.body}>
          {
            hasContacts &&
            <div className={s.collapseStateIndicator}/>
          }

          <div className={s.mainContent}>
            <div className={s.fullName}>
              {profile.fullName}
            </div>

            <div className={s.cityName}>
              {profile.cityName}
            </div>

            <div className={s.phoneNumber}>
              {profile.phoneNumber}
            </div>
          </div>

          {
            onDeleteClick &&
            <ScButton
              className={s.deleteButton}
              visualType={SC_BUTTON_VISUAL_TYPES.ROUNDED}
              text={"Delete"}
              onClick={this.handleDeleteClick}/>
          }
        </div>

        {
          !isCollapsed &&
          profile.contactList.size > 0 &&
          <NestedProfileSummaryRowListView
            depthLevel={depthLevel}
            profileList={profile.contactList}
            onDeleteProfileClick={onDeleteClick}/>
        }
      </div>
    );
  }
}

export default NestedProfileSummaryRow;
