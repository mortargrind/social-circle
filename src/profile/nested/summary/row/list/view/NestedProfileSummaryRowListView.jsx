import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";
import classNames from "classnames";

import {
  isEven,
  isOdd,
} from "../../../../../../common/math/mathUtils";
import NestedProfileSummaryRow from "../../NestedProfileSummaryRow";

import s from "./_nested-profile-summary-row-list-view.scss";

class NestedProfileSummaryRowListView extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    profileList: ImmutablePropTypes.list.isRequired,
    depthLevel: PropTypes.number,
    onDeleteProfileClick: PropTypes.func,
  }

  render() {
    const {
      className,
      profileList,
      depthLevel,
      onDeleteProfileClick,
    } = this.props;
    const rootClassName = classNames(
      s.root,
      className,
      {
        [s.hasEvenDepth]: (isEven(depthLevel)),
        [s.hasOddDepth]: (isOdd(depthLevel)),
      }
    );

    return (
      <div className={rootClassName}>
        <ul className={s.list}>
          {profileList.map(
            profile => (
              <li
                key={profile.id}
                className={s.listItem}>
                <NestedProfileSummaryRow
                  depthLevel={depthLevel + 1}
                  profile={profile}
                  onDeleteClick={onDeleteProfileClick}/>
              </li>
            ))}
        </ul>
      </div>
    );
  }
}

export default NestedProfileSummaryRowListView;
