import React, {PureComponent} from "react";
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";
import {connect} from "react-redux";

import {actionCreators as profileActionCreators} from "../../../profileActions";
import ScPageContent from "../../../../component/page/content/ScPageContent";
import ErrorBoundary from "../../../../main/error/boundary/ErrorBoundary";
import NestedProfileSummaryRowListView
  from "../../../nested/summary/row/list/view/NestedProfileSummaryRowListView";

import s from "./_delete-profile-page-content.scss";

class DeleteProfilePageContent extends PureComponent {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nestedProfileList: ImmutablePropTypes.list,
  }

  handleDeleteProfileClick = (event, profile) => {
    const {dispatch} = this.props;

    dispatch(profileActionCreators.deleteNestedProfile(profile.id));
  }

  render() {
    const {nestedProfileList} = this.props;

    return (
      <ScPageContent
        className={s.root}
        toggleClassNames={s.page}>

        <ErrorBoundary>
          {
            (nestedProfileList && nestedProfileList.size > 0) ?
              <NestedProfileSummaryRowListView
                depthLevel={0}
                profileList={nestedProfileList}
                onDeleteProfileClick={this.handleDeleteProfileClick}/> :
              <div className={s.emptyContentMessage}>
                {"No profiles left to delete. :("}
              </div>
          }
        </ErrorBoundary>
      </ScPageContent>
    );
  }
}

function mapStateToProps(state) {
  const {profileState: {nestedProfileList}} = state;

  return {
    nestedProfileList,
  };
}

const ConnectedDeleteProfilePageContent = connect(mapStateToProps)(DeleteProfilePageContent);

export default ConnectedDeleteProfilePageContent;
