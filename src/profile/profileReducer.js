import {
  generateNestedProfileListFromProfileMap,
  generateProfileMapFromRawProfiles,
  generateContactPathForProfile,
  deleteProfileFromContactList,
  findIndexOfProfile,
} from "./profileUtils";
import {
  DUMMY_RAW_PROFILE_ARRAY
} from "./profileConstants";

function generateDefaultInitialProfileState() {
  const profileMap = generateProfileMapFromRawProfiles(
    DUMMY_RAW_PROFILE_ARRAY
  );

  return {
    profileMap,
    nestedProfileList: generateNestedProfileListFromProfileMap(
      profileMap,
    )
  };
}

function profileReducer(state = generateDefaultInitialProfileState(), action) {
  let newState;

  switch (action.type) {
    case ("DELETE_NESTED_PROFILE"): {
      const {profileId: deleteCandidateProfileId} = action.payload;
      const deleteCandidateProfile = state.profileMap.get(
        deleteCandidateProfileId,
      );
      const updateCandidateParentProfileWithOutOfDateContacts = state.profileMap.get(
        deleteCandidateProfile.connectedProfileId
      );
      let nestedProfileList, profileMap;

      if (updateCandidateParentProfileWithOutOfDateContacts) {
        const parentPath = generateContactPathForProfile(
          updateCandidateParentProfileWithOutOfDateContacts,
          state.profileMap,
          state.nestedProfileList,
        ).reverse();
        const updateCandidateParentProfile = state.nestedProfileList.getIn(parentPath);
        const updatedParentProfile = deleteProfileFromContactList(
          updateCandidateParentProfile,
          deleteCandidateProfile
        );

        nestedProfileList = state.nestedProfileList.setIn(
          parentPath,
          updatedParentProfile,
        );
        profileMap = state.profileMap.withMutations(
          (mutableMap) => {
            mutableMap.set(updatedParentProfile.id, updatedParentProfile);
            mutableMap.delete(deleteCandidateProfile.id);
          }
        );
      } else {
        nestedProfileList = state.nestedProfileList.delete(
          findIndexOfProfile(state.nestedProfileList, deleteCandidateProfile)
        );
        profileMap = state.profileMap.delete(deleteCandidateProfile.id);
      }

      newState = {
        ...state,
        nestedProfileList,
        profileMap,
      };

      break;
    }

    default:
      newState = state;
  }

  return newState;
}

export {generateDefaultInitialProfileState};
export default profileReducer;
