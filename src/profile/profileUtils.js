import {
  Map as ImmutableMap,
  List as ImmutableList,
  OrderedMap as ImmutableOrderedMap
} from "immutable";

import Profile from "./Profile";

function findIndexOfProfile(profiles, searchedProfile) {
  return profiles.findIndex(
    profile => profile.id === searchedProfile.id
  );
}

function generateProfileMapFromRawProfiles(rawProfileArray) {
  return ImmutableOrderedMap(rawProfileArray.map(
    (rawProfile) => {
      const profile = Profile.fromRawProfile(rawProfile);

      return [profile.id, profile];
    }
  ));
}

function generateContactIdsByProfileMapFromProfileMap(profileMap) {
  const contactIdsByProfileMap = new Map();

  for (const profile of profileMap.values()) {
    if (!contactIdsByProfileMap.has(profile.id)) {
      contactIdsByProfileMap.set(profile.id, []);
    }

    if (profile.connectedProfileId && profileMap.has(profile.connectedProfileId)) {
      if (!contactIdsByProfileMap.has(profile.connectedProfileId)) {
        contactIdsByProfileMap.set(profile.connectedProfileId, []);
      }

      const contactArray = contactIdsByProfileMap.get(profile.connectedProfileId);

      contactArray.push(
        profile.id
      );
    }
  }

  return ImmutableMap(contactIdsByProfileMap);
}

function generateContactPathForProfile(profile, profileMap, nestedProfileList, path = []) {

  if (profile) {
    if (profile.connectedProfileId && profileMap.has(profile.connectedProfileId)) {
      const parentProfile = profileMap.get(profile.connectedProfileId);
      const foundIndex = findIndexOfProfile(parentProfile.contactList, profile);

      if (foundIndex >= 0) {
        path.push(
          foundIndex
        );

        path.push(
          "contactList"
        );

        generateContactPathForProfile(
          parentProfile,
          profileMap,
          nestedProfileList,
          path,
        );
      }
    } else {
      const foundIndex = findIndexOfProfile(nestedProfileList, profile);

      if (foundIndex >= 0) {
        path.push(
          foundIndex
        );
      }
    }
  }

  return path;
}

function generateNestedProfileListFromProfileMap(profileMap) {
  const contactIdsByProfileMap = generateContactIdsByProfileMapFromProfileMap(
    profileMap
  );
  const socialCircleArray = [];

  for (const profile of profileMap.values()) {
    if (
      !profile.connectedProfileId ||
      !profileMap.has(profile.connectedProfileId)
    ) {
      socialCircleArray.push(profile);
    }
    const contactIds = contactIdsByProfileMap.get(profile.id);

    if (contactIds) {
      profile.contactList = ImmutableList(
        contactIds.map(
          contactProfileId => profileMap.get(contactProfileId)
        )
      );
    }
  }

  return ImmutableList(socialCircleArray);
}

function deleteProfileFromContactList(
  parentProfile,
  profile,
) {
  const contactList = parentProfile.contactList.delete(
    findIndexOfProfile(
      parentProfile.contactList,
      profile,
    )
  );

  return {
    ...parentProfile,
    contactList,
  };
}

export {
  generateProfileMapFromRawProfiles,
  generateContactIdsByProfileMapFromProfileMap,
  generateNestedProfileListFromProfileMap,
  generateContactPathForProfile,
  deleteProfileFromContactList,
  findIndexOfProfile,
};
