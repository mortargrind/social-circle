import profileReducer, {generateDefaultInitialProfileState} from "./profileReducer";
import {actionCreators as profileActionCreators} from "./profileActions";
import {
  largeRawProfileArray,
} from "../test/mock/profile/profileMocks";
import {
  generateNestedProfileListFromProfileMap,
  generateProfileMapFromRawProfiles,
} from "./profileUtils";

/* eslint-disable no-magic-numbers */
describe("profileReducer tests", () => {
  it("DELETE_NESTED_PROFILE action should correctly delete a leaf profile", () => {
    const profileMap = generateProfileMapFromRawProfiles(
      largeRawProfileArray,
    );

    const initialState = {
      ...generateDefaultInitialProfileState(),
      profileMap,
      nestedProfileList: generateNestedProfileListFromProfileMap(
        profileMap,
      ),
    };

    const newState = profileReducer(
      initialState,
      profileActionCreators.deleteNestedProfile(
        profileMap.get(4).id
      )
    );

    expect(newState.profileMap.size).toBe(9);
    expect(newState.profileMap.has(profileMap.get(4).id)).toBe(false);
    expect(newState.nestedProfileList.getIn([0, "contactList", 2])).not.toBeDefined();
    expect(newState.nestedProfileList.getIn([0, "contactList"]).size).toBe(
      2
    );
  });

  it("DELETE_NESTED_PROFILE action should correctly delete a root profile", () => {
    const profileMap = generateProfileMapFromRawProfiles(
      largeRawProfileArray,
    );

    const initialState = {
      ...generateDefaultInitialProfileState(),
      profileMap,
      nestedProfileList: generateNestedProfileListFromProfileMap(
        profileMap,
      ),
    };

    const newState = profileReducer(
      initialState,
      profileActionCreators.deleteNestedProfile(
        profileMap.get(10).id
      )
    );

    expect(newState.profileMap.size).toBe(9);
    expect(newState.profileMap.has(profileMap.get(10).id)).toBe(false);
    expect(newState.nestedProfileList.getIn([2])).not.toBeDefined();
    expect(newState.nestedProfileList.size).toBe(2);
  });


  it("DELETE_NESTED_PROFILE action should correctly delete a intermediate profile", () => {
    const profileMap = generateProfileMapFromRawProfiles(
      largeRawProfileArray,
    );

    const initialState = {
      ...generateDefaultInitialProfileState(),
      profileMap,
      nestedProfileList: generateNestedProfileListFromProfileMap(
        profileMap,
      ),
    };

    const newState = profileReducer(
      initialState,
      profileActionCreators.deleteNestedProfile(
        profileMap.get(5).id
      )
    );

    expect(newState.profileMap.size).toBe(9);
    expect(newState.profileMap.has(profileMap.get(5).id)).toBe(false);
    expect(newState.nestedProfileList.getIn([0, "contactList", 2, "contactList", 0])).not.toBeDefined();
    expect(newState.nestedProfileList.getIn([0, "contactList", 2, "contactList"]).size).toBe(0);
  });
});
/* eslint-disable no-magic-numbers */
