import React from "react";
import ReactDOM from "react-dom";
import {Provider as ReduxProvider} from "react-redux";
import {BrowserRouter} from "react-router-dom";

import "./common/ui/base/module.scss";
import App from "./app/App";
import {createReduxStore} from "./main/redux/reduxStoreUtils";

const reduxStore = createReduxStore();
const appContainerElement = document.getElementById("root");

function run() {
  ReactDOM.render(
    <ReduxProvider store={reduxStore}>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </ReduxProvider>,
    appContainerElement,
  );
}

run();
