import {
  List as ImmutableList,
} from "immutable";

const largeRawProfileArray = [
  {
    ID: 1,
    Name: "Ahmet Hamdi Tanpınar",
    City: "İstanbul",
    Phone: "123456",
    parentID: 12,
  },
  {
    ID: 2,
    Name: "Sait Faik Abasıyanık",
    City: "İstanbul",
    Phone: "123456",
    parentID: 1,
  },
  {
    ID: 3,
    Name: "Peyami Safa",
    City: "İzmir",
    Phone: "345345",
    parentID: 1,
  },
  {
    ID: 4,
    Name: "Ömer Seyfettin",
    City: "Ankara",
    Phone: "567890",
    parentID: 1,
  },
  {
    ID: 5,
    Name: "Kemal Tahir",
    City: "Bursa",
    Phone: "1313131",
    parentID: 3,
  },
  {
    ID: 6,
    Name: "Sabahattin Ali",
    City: "Adana",
    Phone: "1313131",
    parentID: 0,
  },
  {
    ID: 7,
    Name: "Reşat Nuri Güntekin",
    City: "Nevşehir",
    Phone: "1313131",
    parentID: 5,
  },
  {
    ID: 8,
    Name: "Yaşar Kemal",
    City: "Artvin",
    Phone: "1313131",
    parentID: 2,
  },
  {
    ID: 9,
    Name: "Murathan Mungan",
    City: "Van",
    Phone: "1313131",
    parentID: 2,
  },
  {
    ID: 10,
    Name: "Orhan Kemal",
    City: "Edirne",
    Phone: "1313131",
  },
];

const profileWith3Contacts = {
  id: 1,
  fullName: "Ahmet Hamdi Tanpınar",
  cityName: "İstanbul",
  phoneNumber: "123456",
  connectedProfileId: 12,
  contactList: ImmutableList([
    {
      id: 2,
      fullName: "Sait Faik Abasıyanık",
      cityName: "İstanbul",
      phoneNumber: "123456",
      connectedProfileId: 1,
    },
    {
      id: 3,
      fullName: "Peyami Safa",
      cityName: "İzmir",
      phoneNumber: "345345",
      connectedProfileId: 1,
    },
    {
      id: 4,
      fullName: "Ömer Seyfettin",
      cityName: "Ankara",
      phoneNumber: "567890",
      connectedProfileId: 1,
    },
  ]),
};

const profileWithoutContacts = {
  id: 8,
  fullName: "Yaşar Kemal",
  cityName: "Artvin",
  phoneNumber: "1313131",
  connectedProfileId: 2,
  contactList: ImmutableList(),
};

const profileWithNestedContacts = {
  id: 3,
  fullName: "Peyami Safa",
  cityName: "İzmir",
  phoneNumber: "345345",
  connectedProfileId: 1,
  contactList: ImmutableList([{
    id: 5,
    fullName: "Kemal Tahir",
    cityName: "Bursa",
    phoneNumber: "1313131",
    connectedProfileId: 3,
    contactList: ImmutableList([
      {
        id: 7,
        fullName: "Reşat Nuri Güntekin",
        cityName: "Nevşehir",
        phoneNumber: "1313131",
      }
    ])
  }]),
};

export {
  largeRawProfileArray,
  profileWith3Contacts,
  profileWithoutContacts,
  profileWithNestedContacts,
};
