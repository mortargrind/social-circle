const webpack = require("webpack");
const path = require("path");
const CleanPlugin = require("clean-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const postCssConfig = require('./postcss.config');

const buildPath = path.resolve(__dirname, "build");
const isDebug = !process.argv.includes("--release");
const clientEntryFiles = [
  path.resolve(__dirname, "src/index.jsx")
];

const config = {
  target: "web",

  mode: isDebug ? "development" : "production",

  entry: {
    client: clientEntryFiles,
  },

  output: {
    path: buildPath,
    publicPath: "/",
    filename: "[name]-[hash].js",
    chunkFilename: "[name]-[chunkhash].js"
  },

  resolve: {
    extensions: ['.jsx', '.mjs', '.js', '.json']
  },

  module: {
    strictExportPresence: true,
    rules: [
      {
        test: /\.(js|jsx|mjs)$/,
        include: [
          path.resolve(__dirname, "src"),
        ],
        loader: "babel-loader",
        options: {
          cacheDirectory: isDebug,
        }
      },
      {
        test: /\.css$/,
        use: [
          isDebug ?
            {
              loader: "style-loader",
              options: {
                insertAt: "top",
                singleton: true,
                sourceMap: false
              }
            } :
            MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: false,
              importLoaders: 1,
              sourceMap: isDebug,
              minimize: isDebug,
            }
          },
          {
            loader: "postcss-loader",
            options: postCssConfig
          }
        ],
      },
      {
        test: /\.scss$/,
        use: [
          isDebug ?
            {
              loader: "style-loader",
              options: {
                insertAt: "top",
                singleton: true,
                sourceMap: false
              }
            } :
            MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: true,
              importLoaders: 1,
              sourceMap: isDebug,
              localIdentName: "[name]-[local]",
              minimize: isDebug,
            }
          },
          {
            loader: "postcss-loader",
            options: postCssConfig
          },
          {
            loader: "sass-loader"
          }
        ],
      },
    ]
  },

  cache: isDebug,

  stats: "errors-only",

  devtool: isDebug ? "cheap-module-inline-source-map" : "source-map",

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        sourceMap: false
      }),
      new OptimizeCSSAssetsPlugin({})
    ],
    namedModules: true,
    noEmitOnErrors: true,
    concatenateModules: true,
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendor",
          chunks: "initial"
        }
      }
    }
  },

  plugins: [
    new CleanPlugin(
      [buildPath],
      {
        dry: true,
        verbose: true,
      }
    ),

    ...(
      isDebug ? [
          new webpack.HotModuleReplacementPlugin(),
        ] :
        [
          new MiniCssExtractPlugin({
            filename: '[name]-[contenthash].css',
            chunkFilename: '[id]-[contenthash].css'
          })
        ]
    ),

    new HtmlWebpackPlugin({
      template: "./src/index.html",
      filename: "index.html"
    }),

    new WriteFilePlugin(),
  ],

  devServer: {
    publicPath: '/',
    compress: true,
    port: 3000,
    hot: true,
    hotOnly: true,
    historyApiFallback: true
  }
};

module.exports = config;
