// Jest configuration
// https://facebook.github.io/jest/docs/en/configuration.html
module.exports = {
  automock: false,

  browser: false,

  bail: true,

  collectCoverageFrom: [
    "src/**/*.{js,jsx}",
    "!**/node_modules/**",
    "!**/vendor/**",
  ],

  coverageReporters: ["text"],

  coverageDirectory: "<rootDir>/coverage",

  globals: {
    window: true,
  },

  moduleFileExtensions: ["js", "json", "jsx"],

  moduleNameMapper: {
    "\\.(css|scss)$": "identity-obj-proxy",
  },

  setupFiles: ["<rootDir>/src/test/setupTests.js"],

  testPathIgnorePatterns: [
    "/node_modules/"
  ]
};
