module.exports = {
  parser: "babel-eslint",

  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    }
  },

  plugins: [
    "react",
    "babel",
    "import",
  ],

  env: {
    browser: true,
    es6: true,
    jest: true,
  },

  globals: {
    module: true,
  },

  settings: {
    "react": {
      "version": "16.5.2",
      "pragma": "React",
    },
    "import/resolver": {
      "node": {
        "extensions": [
          ".js",
          ".jsx"
        ]
      }
    }
  },

  rules: {
    // mistakes
    "no-cond-assign": "error",
    "no-constant-condition": "error",
    "no-control-regex": "error",
    "no-debugger": "error",
    "no-dupe-args": "error",
    "no-dupe-keys": "error",
    "no-duplicate-case": "error",
    "no-empty-character-class": "error",
    "no-empty": "error",
    "no-ex-assign": "error",
    "no-extra-boolean-cast": "error",
    "no-extra-semi": "error",
    "no-func-assign": "error",
    "no-inner-declarations": [
      "error",
      "both"
    ],
    "no-invalid-regexp": "error",
    "no-irregular-whitespace": "error",
    "no-obj-calls": "error",
    "no-regex-spaces": "error",
    "no-sparse-arrays": "error",
    "no-template-curly-in-string": "error",
    "no-unexpected-multiline": "error",
    "no-unreachable": "error",
    "no-unsafe-finally": "error",
    "no-unsafe-negation": "error",
    "use-isnan": "error",
    "valid-jsdoc": "error",
    "valid-typeof": "error",
    // best practices
    "accessor-pairs": "error",
    "array-callback-return": "error",
    "block-scoped-var": "error",
    "class-methods-use-this": ["error", {"exceptMethods": ["render"]}],
    "complexity": [
      "warn",
      {
        max: 6
      }
    ],
    "consistent-return": "error",
    "curly": "error",
    "default-case": "error",
    "dot-location": [
      "error",
      "property"
    ],
    "dot-notation": "error",
    "eqeqeq": [
      "error",
      "smart"
    ],
    "guard-for-in": "error",
    "no-alert": "error",
    "no-caller": "error",
    "no-case-declarations": "error",
    "no-div-regex": "error",
    "no-else-return": "error",
    "no-empty-function": "error",
    "no-empty-pattern": "error",
    "no-eq-null": 0,
    "no-eval": "error",
    "no-extend-native": "error",
    "no-extra-bind": "error",
    "no-extra-label": "error",
    "no-fallthrough": "error",
    "no-floating-decimal": "error",
    "no-global-assign": "error",
    "no-implicit-coercion": [
      "error",
      {
        boolean: false
      }
    ],
    "no-implicit-globals": "error",
    "no-implied-eval": "error",
    // "no-invalid-this": "error", //see "babel" counterpart of this rule below
    "no-iterator": "error",
    "no-labels": "error",
    "no-lone-blocks": "error",
    "no-loop-func": "error",
    "no-magic-numbers": [
      "error",
      {
        ignore: [
          1,
          0,
          -1
        ]
      }
    ],
    "no-multi-spaces": "error",
    "no-multi-str": "error",
    "no-new-func": "error",
    "no-new-wrappers": "error",
    "no-new": "error",
    "no-octal-escape": "error",
    "no-octal": "error",
    "no-param-reassign": "error",
    "no-proto": "error",
    "no-redeclare": [
      "error",
      {
        builtinGlobals: true
      }
    ],
    "no-restricted-properties": "error",
    "no-return-await": "error",
    "no-script-url": "error",
    "no-self-assign": "error",
    "no-self-compare": "error",
    "no-sequences": "error",
    "no-throw-literal": "error",
    "no-unmodified-loop-condition": "error",
    "no-unused-expressions": "error",
    "no-unused-labels": "error",
    "no-useless-call": "error",
    "no-useless-concat": "error",
    "no-useless-return": "error",
    "no-useless-escape": "error",
    "no-void": "error",
    // "no-warning-comments": "warn",
    "no-with": "error",
    "prefer-promise-reject-errors": [
      "error",
      {
        allowEmptyReject: true
      }
    ],
    "require-await": "error",
    "vars-on-top": "error",
    "wrap-iife": [
      "error",
      "inside"
    ],
    "yoda": "error",
    // variables
    // "init-declarations": "warn,
    "no-catch-shadow": "error",
    "no-delete-var": "error",
    "no-label-var": "error",
    // "no-restricted-globals": ["error", []]
    "no-shadow-restricted-names": "error",
    "no-shadow": "error",
    "no-undef-init": "error",
    "no-undef": "error",
    "no-undefined": "error",
    "no-unused-vars": [
      "error",
      {
        args: "none"
      }
    ],
    "no-use-before-define": "error",
    // stylistic
    "array-bracket-spacing": [
      "error",
      "never"
    ],
    "block-spacing": [
      "error",
      "never"
    ],
    "brace-style": [
      "error",
      "1tbs"
    ],
    "camelcase": [
      "error",
      {
        properties: "never"
      }
    ],
    // "capitalized-comments": "warn",
    "comma-dangle": [
      "error",
      "only-multiline"
    ],
    "comma-spacing": [
      "error",
      {
        before: false,
        after: true
      }
    ],
    "comma-style": [
      "error",
      "last"
    ],
    "computed-property-spacing": [
      "error",
      "never"
    ],
    "consistent-this": [
      "error",
      "self"
    ],
    "eol-last": [
      "error",
      "always"
    ],
    "func-call-spacing": [
      "error",
      "never"
    ],
    "func-name-matching": [
      "error",
      "never"
    ],
    "func-names": [
      "error",
      "never"
    ],
    "func-style": [
      "error",
      "declaration",
      {
        allowArrowFunctions: true
      }
    ],
    // id-blacklist": ["error", []],
    "id-length": [
      "error",
      {
        exceptions: [
          "i",
          "e",
          "s",
        ]
      }
    ],
    // "id-match": ["error", "^[a-z]+([A-Z][a-z]+)*$"],
    "indent": "off",
    "indent-legacy": [
      "error",
      2, {
        "SwitchCase": 1
      }
    ],
    "jsx-quotes": [
      "error",
      "prefer-double"
    ],
    "key-spacing": [
      "error",
      {
        beforeColon: false,
        afterColon: true,
        mode: "strict"
      }
    ],
    "keyword-spacing": [
      "error",
      {
        before: true,
        after: true
      }
    ],
    "line-comment-position": [
      "error",
      {
        position: "above",
        ignorePattern: "reason:",
        applyDefaultPatterns: true
      }
    ],
    // "linebreak-style": ["warn", "unix"]
    "lines-around-comment": [
      "error",
      {
        beforeBlockComment: true,
        afterBlockComment: false,
        allowBlockStart: true,
        allowBlockEnd: false,
        allowObjectStart: true,
        allowObjectEnd: false,
        allowArrayStart: true,
        allowArrayEnd: false
      }
    ],
    "lines-around-directive": [
      "error",
      {
        before: "never",
        after: "always"
      }
    ],
    "max-depth": [
      "error",
      {
        max: 4
      }
    ],
    "max-len": [
      "error",
      {
        code: 120,
        tabWidth: 2,
        ignoreComments: false,
        ignoreTrailingComments: true,
        ignoreUrls: true,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreRegExpLiterals: true
      }
    ],
    "max-lines": [
      "warn",
      {
        max: 300,
        skipBlankLines: true,
        skipComments: true
      }
    ],
    "max-nested-callbacks": [
      "error",
      {
        max: 3
      }
    ],
    "max-params": [
      "error",
      {
        max: 5
      }
    ],
    "max-statements-per-line": [
      "error",
      {
        max: 1
      }
    ],
    // "max-statements": ["warn", 10]
    "multiline-ternary": [
      "warn",
      "never"
    ],
    "new-cap": [
      "error",
      {
        newIsCap: true,
        capIsNew: false,
        properties: true
      }
    ],
    "new-parens": "error",
    "newline-after-var": [
      "error",
      "always"
    ],
    // "newline-before-return": "error", newline-after-var inconsistency on fix
    "newline-per-chained-call": [
      "error",
      {
        ignoreChainWithDepth: 2
      }
    ],
    "no-array-constructor": "error",
    "no-bitwise": "error",
    "no-continue": "error",
    // "no-inline-comments": "warn",
    "no-lonely-if": "error",
    "no-mixed-operators": [
      "error"
    ],
    "no-multi-assign": "error",
    "no-multiple-empty-lines": [
      "error",
      {
        max: 2,
        maxEOF: 1,
        maxBOF: 0
      }
    ],
    "no-negated-condition": "error",
    "no-nested-ternary": "error",
    "no-new-object": "error",
    "no-plusplus": [
      "error",
      {
        allowForLoopAfterthoughts: true
      }
    ],
    // "no-restricted-syntax": "warn"
    "no-tabs": "error",
    // "no-ternary": "warn"
    "no-trailing-spaces": "error",
    "no-underscore-dangle": [
      "error",
      {
        allowAfterThis: true,
        allowAfterSuper: true
      }
    ],
    "no-unneeded-ternary": [
      "error",
      {
        defaultAssignment: false
      }
    ],
    "no-whitespace-before-property": "error",
    "object-curly-newline": [
      "error",
      {
        ObjectExpression: {
          multiline: true,
          minProperties: 1
        },
        ObjectPattern: {
          multiline: true
        }
      }
    ],
    // formatting bug var,
    "object-curly-spacing": [
      "error",
      "never"
    ],
    "object-property-newline": [
      "error",
      {
        allowMultiplePropertiesPerLine: false
      }
    ],
    "one-var-declaration-per-line": [
      "error",
      "initializations"
    ],
    // "one-var": ["warn", { "var": "always", "let": "never", "const": "never" }]
    // "operator-assignment": ["warn", "always"],
    "operator-linebreak": [
      "error",
      "after"
    ],
    // "padded-blocks": ["warn", "never"]
    "quote-props": [
      "error",
      "consistent-as-needed"
    ],
    "quotes": [
      "error",
      "double",
      {
        allowTemplateLiterals: true,
        avoidEscape: false
      }
    ],
    // "sort-keys": ["warn"],
    // "sort-vars": ["warn"]
    "semi": [
      "error",
      "always"
    ],
    "semi-spacing": [
      "error",
      {
        before: false,
        after: true
      }
    ],
    "space-before-blocks": [
      "error",
      {
        functions: "always",
        keywords: "always",
        classes: "always"
      }
    ],
    // "require-jsdoc": ["warn"],
    "space-before-function-paren": [
      "error",
      {
        anonymous: "always",
        named: "never",
        asyncArrow: "always"
      }
    ],
    "space-infix-ops": [
      "error",
      {
        int32Hint: false
      }
    ],
    "space-in-parens": [
      "error",
      "never"
    ],
    "space-unary-ops": [
      "error",
      {
        words: true,
        nonwords: false,
        overrides: {}
      }
    ],
    "spaced-comment": [
      "error",
      "always"
    ],
    "template-tag-spacing": [
      "error",
      "never"
    ],
    "unicode-bom": [
      "error",
      "never"
    ],
    "wrap-regex": "error",

    // es6
    "arrow-body-style": [
      "error",
      "as-needed",
      {
        requireReturnForObjectLiteral: false
      }
    ],
    "arrow-parens": [
      "error",
      "as-needed",
      {
        requireForBlockBody: true
      }
    ],
    "arrow-spacing": [
      "error",
      {
        before: true,
        after: true
      }
    ],
    "constructor-super": "error",
    "generator-star-spacing": [
      "error",
      {
        before: false,
        after: true
      }
    ],
    "no-class-assign": "error",
    "no-confusing-arrow": [
      "error",
      {
        allowParens: false
      }
    ],
    "no-const-assign": "error",
    "no-dupe-class-members": "error",
    "no-duplicate-imports": [
      "error",
      {
        includeExports: true
      }
    ],
    "no-new-symbol": "error",
    // "no-restricted-imports": ["warn", {"paths": [], "patterns": []}],
    "no-this-before-super": "error",
    "no-useless-computed-key": "error",
    "no-useless-constructor": "error",
    "no-useless-rename": [
      "error",
      {
        ignoreDestructuring: true,
        ignoreImport: true,
        ignoreExport: true
      }
    ],
    "no-var": "error",
    "object-shorthand": [
      "error",
      "always"
    ],
    "prefer-arrow-callback": [
      "error",
      {
        allowUnboundThis: true
      }
    ],
    "prefer-const": "error",
    "prefer-destructuring": [
      "error",
      {
        array: false,
        object: true
      },
      {
        enforceForRenamedProperties: false
      }
    ],
    // prefer-numeric-literals: ["warn"],
    "prefer-rest-params": "error",
    "prefer-spread": "error",
    "prefer-template": "error",
    "require-yield": "error",
    "rest-spread-spacing": [
      "error",
      "never"
    ],
    // "sort-imports": ["warn"]
    "symbol-description": [
      "error"
    ],
    "template-curly-spacing": [
      "error",
      "never"
    ],
    "yield-star-spacing": [
      "error",
      "after"
    ],

    "react/default-props-match-prop-types": [
      "error",
      {
        "allowRequiredDefaults": true
      }
    ],
    "react/destructuring-assignment": [
      "error",
      "always"
    ],
    "react/forbid-component-props": [
      "error",
      {
        "forbid": []
      }
    ],
    "react/forbid-dom-props": [
      "error",
      {
        "forbid": []
      }
    ],
    "react/forbid-elements": [
      "off"
    ],
    "react/forbid-prop-types": [
      "error",
      {
        "forbid": [
          "any"
        ]
      }
    ],
    "react/forbid-foreign-prop-types": [
      "error"
    ],
    "react/no-access-state-in-setstate": [
      "error"
    ],
    "react/no-array-index-key": [
      "error"
    ],
    "react/no-children-prop": [
      "error"
    ],
    "react/no-danger": [
      "error"
    ],
    "react/no-danger-with-children": [
      "error"
    ],
    "react/no-deprecated": [
      "warn"
    ],
    "react/no-did-mount-set-state": [
      "error"
    ],
    "react/no-did-update-set-state": [
      "error"
    ],
    "react/no-direct-mutation-state": [
      "error"
    ],
    "react/no-find-dom-node": [
      "error"
    ],
    "react/no-is-mounted": [
      "error"
    ],
    "react/no-multi-comp": [
      "error"
    ],
    "react/no-redundant-should-component-update": [
      "error"
    ],
    "react/no-render-return-value": [
      "error"
    ],
    "react/no-set-state": [
      "off"
    ],
    "react/no-typos": [
      "error"
    ],
    "react/no-string-refs": [
      "error"
    ],
    "react/no-this-in-sfc": [
      "error"
    ],
    "react/no-unescaped-entities": [
      "error"
    ],
    "react/no-unknown-property": [
      "error"
    ],
    "react/no-unused-prop-types": [
      "error"
    ],
    "react/no-unused-state": [
      "error"
    ],
    "react/no-will-update-set-state": [
      "error"
    ],
    "react/prefer-es6-class": [
      "error",
      "always"
    ],
    "react/prefer-stateless-function": [
      "off"
    ],
    "react/prop-types": [
      "error"
    ],
    "react/react-in-jsx-scope": [
      "error"
    ],
    "react/require-default-props": [
      "off"
    ],
    "react/require-optimization": [
      "error"
    ],
    "react/require-render-return": [
      "error"
    ],
    "react/self-closing-comp": [
      "error",
      {
        "component": true,
        "html": true
      }
    ],
    "react/sort-comp": [
      "error",
      {
        "order": [
          "type-annotations",
          "static-methods",
          "lifecycle",
          "everything-else",
          "rendering"
        ],
        "groups": {
          "rendering": [
            "/^render.+$/",
            "render"
          ]
        }
      }
    ],
    "react/sort-prop-types": [
      "off",
      {
        "callbacksLast": true,
        "ignoreCase": false,
        "requiredFirst": true,
        "sortShapeProp": true
      }
    ],
    "react/style-prop-object": [
      "error"
    ],
    "react/void-dom-elements-no-children": [
      "error"
    ],
    "react/jsx-boolean-value": [
      "error",
      "always"
    ],
    "react/jsx-closing-bracket-location": [
      "error",
      {
        "nonEmpty": "after-props",
        "selfClosing": "after-props"
      }
    ],
    "react/jsx-closing-tag-location": [
      "error"
    ],
    "react/jsx-curly-spacing": [
      "error",
      "never",
      {
        "allowMultiline": true,
        "spacing": {
          "objectLiterals": "never",
          "children": "never",
          "attributes": "never"
        }
      }
    ],
    "react/jsx-equals-spacing": [
      "error",
      "never"
    ],
    "react/jsx-filename-extension": [
      "error",
      {
        "extensions": [
          ".jsx"
        ]
      }
    ],
    "react/jsx-first-prop-new-line": [
      "error",
      "multiline-multiprop"
    ],
    "react/jsx-handler-names": [
      "error",
      {
        "eventHandlerPrefix": "handle",
        "eventHandlerPropPrefix": "on"
      }
    ],
    "react/jsx-indent": [
      "error",
      2
    ],
    "react/jsx-indent-props": [
      "off"
    ],
    "react/jsx-key": [
      "error"
    ],
    "react/jsx-max-props-per-line": [
      "error",
      {
        "maximum": 1,
        "when": "always"
      }
    ],
    "react/jsx-no-bind": [
      "error",
      {
        "ignoreRefs": false,
        "allowArrowFunctions": false,
        "allowFunctions": false,
        "allowBind": false
      }
    ],
    "react/jsx-no-comment-textnodes": [
      "error"
    ],
    "react/jsx-no-duplicate-props": [
      "error"
    ],
    "react/jsx-no-literals": [
      "error"
    ],
    "react/jsx-no-target-blank": [
      "error"
    ],
    "react/jsx-no-undef": [
      "error",
      {
        "allowGlobals": false
      }
    ],
    "react/jsx-one-expression-per-line": [
      "error"
    ],
    "react/jsx-curly-brace-presence": [
      "error",
      {
        "props": "always",
        "children": "ignore"
      }
    ],
    "react/jsx-sort-props": [
      "error",
      {
        "noSortAlphabetically": true,
        "reservedFirst": true
      }
    ],
    "react/jsx-tag-spacing": [
      "error",
      {
        "closingSlash": "never",
        "beforeSelfClosing": "never",
        "afterOpening": "never",
        "beforeClosing": "never"
      }
    ],
    "react/jsx-pascal-case": [
      "error",
      {
        "allowAllCaps": false,
        "ignore": []
      }
    ],
    "react/jsx-sort-default-props": [
      "off"
    ],
    "react/jsx-uses-react": [
      "error"
    ],
    "react/jsx-uses-vars": [
      "error"
    ],
    "react/jsx-wrap-multilines": [
      "error",
      {
        "declaration": "parens-new-line",
        "assignment": "parens-new-line",
        "return": "parens-new-line",
        "arrow": "parens-new-line",
        "condition": "ignore",
        "logical": "ignore",
        "prop": "parens-new-line"
      }
    ],

    //import
    "import/newline-after-import": [
      "error",
      {
        "count": 1
      }
    ],
    "import/no-nodejs-modules": [
      "error"
    ],
    "import/no-commonjs": [
      "error"
    ],
    "import/default": [
      "error"
    ],
    "import/order": [
      "error",
      {
        "groups": [
          [
            "external",
            "internal"
          ],
          [
            "sibling",
            "parent"
          ]
        ],
        "newlines-between": "always-and-inside-groups"
      }
    ],
    "import/no-amd": [
      "error"
    ],
    "import/unambiguous": [
      "error"
    ],
    "import/no-unresolved": [
      "error"
    ],
    "import/named": [
      "error"
    ],
    "import/no-webpack-loader-syntax": [
      "error"
    ],
    "import/no-extraneous-dependencies": [
      "error",
      {
        "devDependencies": true,
        "optionalDependencies": true,
        "peerDependencies": false
      }
    ]
  }
};
