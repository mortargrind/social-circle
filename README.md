# Social Circle

## Setup
* This project requires **Node** >= `8.12.x` and **yarn** > `1.x` for proper development environment.
* Run `yarn install` at the root project folder.

## Development
* Run `yarn run start` or `yarn run build:dev:live` to startup the development server.
* Run `yarn run build:dev` to create the development bundle.
* Run `yarn run build:release` to create the release bundle.
* Run `yarn run lint:js` to execute static analysis results for `js(x)` files.
* Run `yarn run lint:style` to execute static analysis results for `scss` files.
* Run `yarn run fix:js` to execute static analysis and apply auto-fixes for `js(x)` files.
* Run `yarn run fix:style` to execute static analysis results and apply auto-fixes for `scss` files.
* Run `yarn run test` to execute all test cases.
* Run `yarn run health-check` to execute all static analysis checks and tests cases.

## About

* Nested items are named as `profile` and all `profile` objects have potential `contactList` collections; an immutable list of `contact` objects
(which is a short term for `profile` objects that are connected to another `profile` via `connectedProfileId`). 

* All re-usable components (residing in `component` folder); 
that have low-level, feature-independent logic are named with a `Sc` prefix. 
It's an abbreviation of the project's name: `Social Circle`; 
to prevent (mental) naming collisions with other internal or external components and 
emphasizes their nature of "generic" usage.

* We accept "raw profile" objects from outside world and process them to create our own minted `profile` objects: 
Those `profile` objects have proper **camelCase** namings to adhere project's naming standards 
and also utilizes immutable data structures a little bit; to be able 
to notify React & Redux only necessary via simple & efficient reference checks.

## Technical Debt

* Profile factory can be updated to return ImmutableJS Records, instead of plain JS objects to 
have more robust immutability. Right now, there's no real reasoning behind it being a `class`, since it only constructs,
plain objects.

* There may be more efficient ways to store & manage nested data; like using memoized selectors or 
circular references. Managing one map & one list seemed simple & efficient enough to satisfy the requirements. 
The `profileMap` collection is basically there to make the search & find operations faster when there's too many items 
in the nested item list, by acting as a reference point when finding the correct paths to the nested elements.

# Known Issues

* `react-hot-loader` and `React.PureComponent` seems to be not working properly all the time. 
Generally speaking, you may need to refresh the page since hot-loading does not force re-render all the time.

* CSS hot-loading is not working correctly also; due to Style loader & CSS Modules incompatibilities.
(See: https://github.com/webpack-contrib/style-loader/issues/320)
