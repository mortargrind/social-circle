module.exports = function (api) {
  const env = api.env();

  const basePlugins = [
    "@babel/plugin-proposal-class-properties",
  ];

  const basePresets = [];

  let presets = [...basePresets];
  let plugins = [...basePlugins];

  if (env === "test") {
    presets.push([
      "@babel/preset-env",
      {
        "useBuiltIns": "entry",
      }
    ]);
  } else {
    if (env === "production") {
      plugins.push([
        "transform-react-remove-prop-types",
        {
          "mode": "remove",
          "removeImport": true,
          "additionalLibraries": [
            "react-immutable-proptypes"
          ]
        }
      ])
    } else {
      plugins.push("react-hot-loader/babel");
    }

    presets.push(
      [
        "@babel/preset-env",
        {
          "useBuiltIns": "entry",
          "modules": false,
        }
      ]
    );
  }

  presets.push(
    "@babel/preset-react",
  );

  return {
    presets,
    plugins,
  };
};
